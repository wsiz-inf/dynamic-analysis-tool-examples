import { Engine, SubscriberType } from 'sandbox';
import { ExampleBase } from './exampleBase';
import * as ace from 'brace';

const Range = ace.acequire('ace/range').Range;

const sourceCode = `
var street = 'Kosmonautów';
var houseNumber = '11';
var flatNumber;

var address = street + ' ' + houseNumber + '/' + flatNumber;
address = street + ' ' + houseNumber + '/';
address += flatNumber;
address = street + ' ' + houseNumber + (flatNumber ? '/' + flatNumber : '');
`.trim();

export class UndefinedConcatenating extends ExampleBase {
    constructor(container) {
        super(container);

        this.engine = new Engine();
    }

    get title() {
        return 'String and undefined concatenating';
    }

    configure() {
        super.configure();

        this._markers = [];
        this.editor.on('change', () => {
            this._removeMarkers();
        });

        this._setSourceCode(sourceCode);
        this._attachToEngine();
        this._onRun(() => {
            this._run();
            this._processData();
        });
    }

    _run() {
        const sourceCode = this.editor.getValue();
        this._reset();
        this.engine.execute(sourceCode);
    }

    _attachToEngine() {
        const isAnyArgumentUndefined = e => e.left === void 0 || e.right === void 0;

        this.engine.getSubscriberFor(SubscriberType.BinaryExpr)
            .on('fire', e => {
                if (e.operator !== '+') return;
                if (typeof e.returnedValue === 'string' && isAnyArgumentUndefined(e)) {
                    this._data.push(e.node.loc);
                }
            });
        this.engine.getSubscriberFor(SubscriberType.Assign)
            .on('fire', e => {
                if (e.operator !== '+=') return;
                if (typeof e.value === 'string' && isAnyArgumentUndefined(e)) {
                    this._data.push(e.node.loc);
                }
            });
    }

    _processData() {
        this._markers = this._data.map(loc => {
            const { start, end } = loc;
            const range = new Range(start.line - 1, start.column, end.line - 1, end.column);
            return this.editor.session.addMarker(range, 'undefined-concatenating-marker', 'line');
        });
    }

    _reset() {
        this._removeMarkers();
        this._markers = [];
        this._data = [];
    }

    _removeMarkers() {
        this._markers.forEach(markerId => this.editor.session.removeMarker(markerId));
    }
}

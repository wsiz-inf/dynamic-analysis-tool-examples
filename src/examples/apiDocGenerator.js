import * as uuid from 'uuid/v4';
import * as codeGen from 'escodegen';
import * as replace from 'ast-replace';
import { parseScript } from 'esprima';
import { createEditor } from './../editor';
import { Engine, SubscriberType } from 'sandbox';
import { ExampleBase } from './exampleBase';
import 'brace/mode/typescript';
import 'brace/theme/monokai';

const sourceCode = `
function sumOrConcatenate(a, b) {
    return a + b;
}

var string = sumOrConcatenate('a', 'b');
var number = sumOrConcatenate(1, 2);
`.trim();

export class ApiDocGenerator extends ExampleBase {
    constructor(container) {
        super(container);

        this._outputContainerId = uuid();
        this.engine = new Engine();
    }

    get title() {
        return 'API documentation generator';
    }

    configure() {
        super.configure();

        this.outputEditor = createEditor(this._outputContainerId, {
            mode: 'ace/mode/typescript',
            theme: 'ace/theme/monokai'
        });
        this.outputEditor.setReadOnly(true);

        this._setSourceCode(sourceCode);
        this._attachToEngine();
        this._onRun(() => {
            this._run();
            const processedData = this._processData();
            this._setOutput(processedData);
        });
    }

    _run() {
        const sourceCode = this.editor.getValue();
        this._reset();
        this.engine.execute(sourceCode);
    }

    _attachToEngine() {
        this.engine.getSubscriberFor(SubscriberType.Call)
            .on('leave', e => {
                this._data.push({
                    name: e.name,
                    argumentsType: e.arguments.map(a => typeof a),
                    returnType: typeof e.returnedValue
                });
            });
    }

    _processData() {
        const groupedData = this._data.reduce((prev, curr) => {
            prev[curr.name] = prev[curr.name] || {
                argumentsType: [],
                returnTypes: new Set()
            };
            prev[curr.name].returnTypes.add(curr.returnType);
            curr.argumentsType.forEach((type, index) => {
                prev[curr.name].argumentsType[index] = prev[curr.name].argumentsType[index] || new Set();
                prev[curr.name].argumentsType[index].add(type);
            });
            return prev;
        }, {});
        const sourceCode = this.editor.getValue();
        const ast = parseScript(sourceCode);
        const newAst = replace(ast, {
            FunctionDeclaration: {
                test: node => groupedData.hasOwnProperty(node.id.name),
                replace: node => {
                    node.leadingComments = [
                        {
                            'type': 'Block',
                            'value': this._generateJsDocComment(node, groupedData[node.id.name])
                        }
                    ];
                    return node;
                }
            }
        });
        return codeGen.generate(newAst, { comment: true });
    }

    _setOutput(output) {
        this.outputEditor.setValue(output);
        this.outputEditor.clearSelection();
    }

    _reset() {
        this._data = [];
    }

    _configureView() {
        super._configureView();

        const output = document.createElement('pre');
        output.id = this._outputContainerId;
        this._container.appendChild(output);
    }

    _generateJsDocComment(funcDeclaration, data) {
        const params = funcDeclaration.params.map((p, i) => {
            const types = data.argumentsType[i];
            let typesText = Array.from(types).join('|');
            typesText = types.size > 1 ? `(${typesText})` : typesText;
            return `@param {${typesText}} ${p.name}`;
        });

        let typesText = Array.from(data.returnTypes).join('|');
        typesText = data.returnTypes.size > 1 ? `(${typesText})` : typesText;
        const returns = `@returns {${typesText}}`;

        const commentLines = [...params, returns];
        const comment = ['*', ...commentLines.map(l => ` * ${l}`), ' '].join('\n');

        return comment;
    }
}

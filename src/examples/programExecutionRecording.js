import { Engine, SubscriberType } from 'sandbox';
import { ExampleBase } from './exampleBase';
import * as ace from 'brace';
import prettyprint from 'prettyprint';

const Range = ace.acequire('ace/range').Range;

const sourceCode = `
function fibonacci(n) {
    if (n <= 2) {
        return 1;
    }
    var fib = 1;
    var prev = 1;
    var temp;
    for (var i = 2; i < n; ++i) {
        temp = fib;
        fib += prev;
        prev = temp;
    }
    return fib;
}

var result = fibonacci(4);
`.trim();

export class ProgramExecutionRecording extends ExampleBase {
    constructor(container) {
        super(container);

        this.engine = new Engine();
    }

    get _runButtonText() {
        return 'Execute/record';
    }

    get title() {
        return 'Program execution recording';
    }

    configure() {
        super.configure();

        this.editor.on('change', () => {
            this._backwardButton.setAttribute('disabled', 'disabled');
            this._forwardButton.setAttribute('disabled', 'disabled');
        });

        this._setSourceCode(sourceCode);
        this._attachToEngine();
        this._onRun(() => {
            this._run();
        });
    }

    _run() {
        const sourceCode = this.editor.getValue();
        this._reset();
        this.engine.execute(sourceCode);

        if (this._data.length > 0) {
            this._forwardButton.removeAttribute('disabled');
        }
    }

    _attachToEngine() {
        this.engine.getSubscriberFor(SubscriberType.Assign)
            .on('fire', e => this._data.push(e));
        this.engine.getSubscriberFor(SubscriberType.BinaryExpr)
            .on('fire', e => this._data.push(e));
        this.engine.getSubscriberFor(SubscriberType.LogicalExpr)
            .on('fire', e => this._data.push(e));
        this.engine.getSubscriberFor(SubscriberType.UnaryExpr)
            .on('fire', e => this._data.push(e));
        this.engine.getSubscriberFor(SubscriberType.UpdateExpr)
            .on('fire', e => this._data.push(e));
        this.engine.getSubscriberFor(SubscriberType.Call)
            .on('enter', e => this._data.push(e))
            .on('leave', e => this._data.push(e));
        this.engine.getSubscriberFor(SubscriberType.ConditionalExpr)
            .on('fire', e => this._data.push(e));
        this.engine.getSubscriberFor(SubscriberType.FunctionDeclaration)
            .on('fire', e => this._data.push(e));
        this.engine.getSubscriberFor(SubscriberType.FunctionExpression)
            .on('fire', e => this._data.push(e));
        this.engine.getSubscriberFor(SubscriberType.Return)
            .on('fire', e => this._data.push(e));
        this.engine.getSubscriberFor(SubscriberType.VariableDeclaration)
            .on('fire', e => this._data.push(e));
        this.engine.getSubscriberFor(SubscriberType.Program)
            .on('enter', e => this._data.push(e))
            .on('leave', e => this._data.push(e));
    }

    _reset() {
        this._data = [];
        this._position = -1;
        this.editor.session.removeMarker(this._marker);

        this._backwardButton.setAttribute('disabled', 'disabled');
        this._forwardButton.setAttribute('disabled', 'disabled');

        this._variablesDump.textContent = '';
    }

    _configureView() {
        super._configureView();

        this._backwardButton = this._addButton('← Backward', () => {
            this._prev();
            if (this._position < 0) {
                this._backwardButton.setAttribute('disabled', 'disabled');
            } else {
                this._forwardButton.removeAttribute('disabled');

                const preparedData = this._getPreparedDataForCurrentNode(this._position);
                this._processPreparedData(preparedData);
            }
        });
        this._forwardButton = this._addButton('Forward →', () => {
            this._next();
            if (this._position === this._data.length) {
                this._forwardButton.setAttribute('disabled', 'disabled');
            } else {
                this._backwardButton.removeAttribute('disabled');

                const preparedData = this._getPreparedDataForCurrentNode(this._position);
                this._processPreparedData(preparedData);
            }
        });

        const description = document.createElement('div');
        description.textContent = 'variables';

        this._variablesDump = document.createElement('pre');
        this._variablesDump.classList.add('variables-dump');

        this._container.appendChild(description);
        this._container.appendChild(this._variablesDump);
    }

    _next() {
        this._position < this._data.length && ++this._position;
    }

    _prev() {
        this._position >= 0 && --this._position;
    }

    _getPreparedDataForCurrentNode(position) {
        const current = this._data[position];

        let context = current.context;
        const values = {};

        while (context !== null) {
            const scopeKeys = Object.keys(values);

            Object.keys(context)
                .filter(k => scopeKeys.indexOf(k) === -1)
                .forEach(k => values[k] = context[k]);

            context = context.__proto__;
        }

        return {
            context: values,
            loc: current.node.loc
        };
    }

    _processPreparedData({ loc: { start, end }, context }) {
        const range = new Range(start.line - 1, start.column, end.line - 1, end.column);

        this.editor.session.removeMarker(this._marker);
        this._marker = this.editor.session.addMarker(range, 'nan-marker', 'line');

        this._variablesDump.textContent = prettyprint(context);
    }
}

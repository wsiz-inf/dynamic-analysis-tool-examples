import * as uuid from 'uuid/v4';
import { createEditor } from './../editor';
import { Engine, SubscriberType } from 'sandbox';
import { ExampleBase } from './exampleBase';
import 'brace/mode/text';
import 'brace/theme/chrome';

const sourceCode = `
function fibonacci(number) {
    if (number === 0)
        return 0;
    if (number === 1)
        return 1;
    return fibonacci(number - 1) + fibonacci(number - 2);
}
fibonacci(4);
`.trim();

export class TreeCall extends ExampleBase {
    constructor(container) {
        super(container);

        this._outputContainerId = uuid();
        this.engine = new Engine();
    }

    get title() {
        return 'Tree of recursive calls';
    }

    configure() {
        super.configure();

        this.outputEditor = createEditor(this._outputContainerId, {
            mode: 'ace/mode/text',
            theme: 'ace/theme/chrome'
        });
        this.outputEditor.setReadOnly(true);

        this._setSourceCode(sourceCode);
        this._attachToEngine();
        this._onRun(() => {
            this._run();
            const processedData = this._data.join('\n');
            this._setOutput(processedData);
        });
    }

    _run() {
        const sourceCode = this.editor.getValue();
        this._reset();
        this.engine.execute(sourceCode);
    }

    _attachToEngine() {
        const log = (indent, str) => this._data.push('\t'.repeat(indent - 1) + str);

        this.engine.getSubscriberFor(SubscriberType.Program)
            .on('enter', e => log(e.indent, `program start`))
            .on('leave', e => log(e.indent, `program end -> ${e.returnedValue}`));
        this.engine.getSubscriberFor(SubscriberType.Call)
            .on('enter', e => {
                const args = e.arguments.map(arg => arg.toString()).join(', ');
                log(e.indent, `${e.name} (${args})`);
            })
            .on('leave', e => log(e.indent, `${e.name} -> ${e.returnedValue}`));
    }

    _setOutput(output) {
        this.outputEditor.setValue(output);
        this.outputEditor.clearSelection();
    }

    _reset() {
        this._data = [];
        this.outputEditor.setValue('');
        this.outputEditor.clearSelection();
    }

    _configureView() {
        super._configureView();

        const output = document.createElement('pre');
        output.id = this._outputContainerId;
        this._container.appendChild(output);
    }
}

import * as uuid from 'uuid/v4';
import { createEditor } from './../editor';
import 'brace/mode/javascript';
import 'brace/theme/monokai';

export class ExampleBase {
    constructor(container) {
        this._container = container;
        this._editorContainerId = uuid();
        this._buttonsContainer = null;
    }

    get _runButtonText() {
        return 'Run';
    }

    get title() {
        throw new Error();
    }

    configure() {
        this._configureView();
        this.editor = createEditor(this._editorContainerId, {
            mode: 'ace/mode/javascript',
            theme: 'ace/theme/monokai'
        });
    }

    run() { }

    _setSourceCode(sourceCode) {
        this.editor.setValue(sourceCode);
        this.editor.clearSelection();
    }

    _onRun(callback) {
        const button = document.getElementById(`run-${this._editorContainerId}`);
        button.addEventListener('click', callback);
    }

    _configureView() {
        const title = document.createElement('h2');
        title.textContent = this.title;
        this._container.appendChild(title);

        const editor = document.createElement('pre');
        editor.id = this._editorContainerId;
        this._container.appendChild(editor);

        const runButton = document.createElement('button');
        runButton.id = `run-${this._editorContainerId}`;
        runButton.textContent = this._runButtonText;

        this._buttonsContainer = document.createElement('div');
        this._buttonsContainer.appendChild(runButton);
        this._container.appendChild(this._buttonsContainer);
    }

    _addButton(text, onclick) {
        const button = document.createElement('button');
        button.id = uuid();
        button.textContent = text;
        button.addEventListener('click', () => onclick());
        this._buttonsContainer.appendChild(button);
        return button;
    }
}

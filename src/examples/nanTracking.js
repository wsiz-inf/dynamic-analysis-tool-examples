import { Engine, SubscriberType } from 'sandbox';
import { ExampleBase } from './exampleBase';
import * as ace from 'brace';

const Range = ace.acequire('ace/range').Range;

const sourceCode = `
function sum(a, b) {
    return a + b;
}

var a = sum(1, undefined);
var b = sum(a, 2);
b = isNaN(b) ? 0 : b;
`.trim();

export class NaNTracking extends ExampleBase {
    constructor(container) {
        super(container);

        this.engine = new Engine();
    }

    get title() {
        return 'NaNs tracking';
    }

    configure() {
        super.configure();

        this._markers = [];
        this.editor.on('change', () => {
            this._removeMarkers();
        });

        this._setSourceCode(sourceCode);
        this._attachToEngine();
        this._onRun(() => {
            this._run();
            this._processData();
        });
    }

    _run() {
        const sourceCode = this.editor.getValue();
        this._reset();
        this.engine.execute(sourceCode);
    }

    _attachToEngine() {
        const isNotANumber = n => isNaN(n) && n !== void 0;

        this.engine.getSubscriberFor(SubscriberType.Call)
            .on('enter', e => {
                e.arguments
                    .map((value, index) => ({ value, index }))
                    .filter(a => isNotANumber(a.value))
                    .forEach(a => this._data.push({ pos: e.node.arguments[a.index].loc }));
            })
            .on('leave', e => {
                if (isNotANumber(e.returnedValue)) {
                    this._data.push({ pos: e.node.callee.loc });
                }
            });
        this.engine.getSubscriberFor(SubscriberType.VariableDeclaration)
            .on('fire', e => {
                if (isNotANumber(e.value)) {
                    this._data.push({ pos: e.node.id.loc });
                }
            });
        this.engine.getSubscriberFor(SubscriberType.Assign)
            .on('fire', e => {
                if (isNotANumber(e.value)) {
                    this._data.push({ pos: e.node.left.loc });
                }
            });
        this.engine.getSubscriberFor(SubscriberType.ConditionalExpr)
            .on('fire', e => {
                if (isNotANumber(e.returnedValue)) {
                    this._data.push({ pos: e.consequent ? e.node.consequent.loc : e.node.alternate.loc });
                }
            });
    }

    _processData() {
        this._markers = this._data.map(({ pos }) => {
            const { start, end } = pos;
            const range = new Range(start.line - 1, start.column, end.line - 1, end.column);
            return this.editor.session.addMarker(range, 'nan-marker', 'line');
        });
    }

    _reset() {
        this._removeMarkers();
        this._markers = [];
        this._data = [];
    }

    _removeMarkers() {
        this._markers.forEach(markerId => this.editor.session.removeMarker(markerId));
    }
}

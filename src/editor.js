import * as ace from 'brace';

export const createEditor = (elementId, { mode, theme }) => {
    const editor = ace.edit(elementId);
    editor.setTheme(theme);
    editor.getSession().setMode(mode);
    editor.setShowPrintMargin(false);
    editor.renderer.setScrollMargin(10, 10);
    return editor;
};

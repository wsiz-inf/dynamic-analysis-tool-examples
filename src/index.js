import { TreeCall } from './examples/treeCall';
import { NaNTracking } from './examples/nanTracking';
import { UndefinedConcatenating } from './examples/undefinedConcatenating';
import { ApiDocGenerator } from './examples/apiDocGenerator';
import { ProgramExecutionRecording } from './examples/programExecutionRecording';

const container = document.getElementById('container');
const examples = [
    new TreeCall(container),
    new NaNTracking(container),
    new UndefinedConcatenating(container),
    new ApiDocGenerator(container),
    new ProgramExecutionRecording(container)
];

examples.forEach(e => e.configure());

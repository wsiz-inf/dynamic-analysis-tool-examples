const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const path = require('path');

// paths
const srcPath = path.resolve(__dirname, './src');
const distPath = path.resolve(__dirname, './dist');
const nodeModulesPath = path.resolve(__dirname, './node_modules');

module.exports = () => {
    const config = {
        entry: {
            index: `${srcPath}/index.js`,
        },
        output: {
            path: distPath,
            filename: `[name].js`
        },
        devtool: 'source-map',
        resolve: {
            extensions: ['.js'],
            modules: [srcPath, nodeModulesPath]
        },
        module: {
            rules: [
                {
                    test: /\.html$/,
                    loader: 'raw-loader'
                }
            ]
        },
        plugins: [
            // new UglifyJsPlugin(),
            new CopyWebpackPlugin([
                {
                    from: `${srcPath}/index.html`,
                    to: distPath
                },
                {
                    from: `${srcPath}/index.css`,
                    to: distPath
                }
            ])
        ]
    };

    return config;
};
